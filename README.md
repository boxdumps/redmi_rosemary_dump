## rosemary-user 11 RP1A.200720.011 V12.5.13.0.RKLMIXM release-keys
- Manufacturer: xiaomi
- Platform: mt6785
- Codename: rosemary
- Brand: Redmi
- Flavor: rosemary-user
maltose-user
rosemary-user
secret-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: V12.5.13.0.RKLMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/rosemary/rosemary:11/RP1A.200720.011/V12.5.13.0.RKLMIXM:user/release-keys
- OTA version: 
- Branch: rosemary-user-11-RP1A.200720.011-V12.5.13.0.RKLMIXM-release-keys
- Repo: redmi_rosemary_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
